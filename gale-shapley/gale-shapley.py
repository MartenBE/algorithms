
#!/usr/bin/env python3

# Now, let’s execute the Gale-Shapley algorithm, with men as proposers.
# Aladdin proposes to Jasmine, Eric and Prince Charming propose to Cinderella, The Beast proposes to Ariel.
# Jasmine provisionally accepts Aladdin, Cinderella provisionally accepts Prince Charming, and Ariel provisionally accepts The Beast.
# Now Eric is free, so he proposes to Ariel. Ariel rejects The Beast, and provisionally accepts Eric.
# Now The Beast is free, so he proposes to Belle. She provisionally accepts him.
# No one is free, so the algorithm terminates. Everyone is married to his or her provisional partner. So Jasmine is with Aladdin, Eric is with Ariel, Cinderella is with Prince Charming, and The Beast is with Belle.
# Everything is as it should be in the Magic Kingdom.

# http://www.sccs.swarthmore.edu/users/06/rshorey/math/execute.html

import glob
import os

persons = []
files = glob.glob("*.txt")

for f in files:
	person = os.path.splitext(os.path.basename(f))[0]
	persons.append(person)

print(persons)

for f in files:
	pf = file("test.txt", 'r')
	i = 0
	for line in pf:
		
		i++
	f.close() 
