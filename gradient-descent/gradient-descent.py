
#!/usr/bin/env python3

# y = a * x + b

from random import random, randint
import matplotlib.pyplot as plt
import csv

class Point:
    def __init__(self, x, y):
        self.x = x;
        self.y = y;

    def __str__(self):
        return "Point@({}, {})".format(self.x, self.y)


def calculate_error(a, b, points):
    error = 0.0
    for p in points:
        error += (p.y - (a*p.x + b))**2
    error /= (2*len(points))
    return error

def batch_gradient_descent(points, max_loops, precision, gamma):
    a = random()
    b = random()
    errors = [calculate_error(a, b, points)]

    i = 0
    while i < max_loops and (i == 0 or abs(errors[-1] - errors[-2]) > precision):
        # if i > 0:
        #     print("{:5d} | difference error: {:.2f}".format(i, abs(errors[-1] - errors[-2])))

        a_grad = 0.0
        b_grad = 0.0
        for p in points:
            a_grad += p.x * ((a * p.x + b) - p.y)
            b_grad += ((a * p.x + b) - p.y)
        a_grad /= len(points)
        b_grad /= len(points)

        a = a - gamma * a_grad
        b = b - gamma * b_grad
        
        error = calculate_error(a, b, points)
        errors.append(error)
        i += 1

    return (i, errors, a, b)

def stochastic_gradient_descent(points, max_loops, precision, gamma):
    a = random()
    b = random()
    errors = [calculate_error(a, b, points)]

    i = 0
    while i < max_loops and (i == 0 or abs(errors[-1] - errors[-2]) > precision):  
        for j, p in enumerate(points):
            # if i > 0:
            #     print("{:5d} {:5d} | difference error: {:.2f}".format(i, j, abs(errors[-1] - errors[-2])))

            a = a - gamma * (p.x * ((a * p.x + b) - p.y))
            b = b - gamma * ((a * p.x + b) - p.y)
            
            error = calculate_error(a, b, points)
            errors.append(error)
        i += 1

    return (i * j, errors, a, b)
    
# TODO
def minibatch_gradient_descent(points, max_loops, precision, gamma):
    a = random()
    b = random()
    errors = [calculate_error(a, b, points)]

    i = 0
    while i < max_loops and (i == 0 or abs(errors[-1] - errors[-2]) > precision):  
        # change this for loop too
        # batches = split dataset into mini-batches
        # for batch in batches: 
        for p in points:
            # if i > 0:
            #     print("{:5d} {:5d} | difference error: {:.2f}".format(i, j, abs(errors[-1] - errors[-2])))

            a = a - gamma * (p.x * ((a * p.x + b) - p.y))
            b = b - gamma * ((a * p.x + b) - p.y)
            
            error = calculate_error(a, b, points)
            errors.append(error)
        i += 1

    return (i, errors, a, b)

################################################################################

def test_gradient_descent(func, max_loops, precision, gamma):
    print("Calculating the lineair regression by using gradient descent, this could take a while...")
    print("\tMax loops:", max_loops)
    print("\tPrecision:", precision)
    print("\tGamma:", gamma)
    
    try:
        (iterations, errors, a, b) = batch_gradient_descent(points, max_loops, precision, gamma)

        print("Done")
        print("Amount of iterations:", iterations)
        print("Result:", a, "* x + ", b)

        plt.scatter(
            [p.x for p in points],
            [p.y for p in points]
        )
        plt.plot(
            [p.x for p in points],
            [(a * p.x + b) for p in points],
            color='red'
        )
        plt.show()
        plt.close()

        plt.plot(errors)
        plt.show()
        plt.close()

        print(a*3.5 + b, a*7 + b)

    except OverflowError:
        print()
        print("The gradien descent algorithm exploded and therefore failed!")
        print("Try adjusting the parameters")


# http://openclassroom.stanford.edu/MainFolder/DocumentPage.php?course=MachineLearning&doc=exercises/ex2/ex2.html
# x = age, y = height
# After 1 time: a = 0.3800,b = 0.0745
# After convergence: a = 0.0639, b = 0.7502
# The predicted height for age 3.5 is 0.9737 meters, and for age 7 is 1.1975 meters

with open('data-points.csv') as csvfile:
    csv_reader = csv.DictReader(csvfile)
    points = [ Point(float(row['x']), float(row['y'])) for row in csv_reader ]

    print()
    print("Batch gradient descent")
    print("----------------------")
    test_gradient_descent(batch_gradient_descent, 5000, 1e-10, 0.07)
    print()

    print("Stochastic gradient descent")
    print("---------------------------")
    test_gradient_descent(stochastic_gradient_descent, 5000, 1e-10, 0.07)
    print()