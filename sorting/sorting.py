
#!/bin/python

from random import randint
import matplotlib.pyplot as plt

def swap(a, b):
    a, b = b, a

def create_random_list(size, min_value, max_value):
    return [randint(min_value, max_value) for i in range(size)]

def check_is_list_ordered(ordered_list):
    i = 1
    is_ordered = True
    while i < len(ordered_list) and is_ordered:
        is_ordered = ordered_list[i-1] <= ordered_list[i]
        i += 1
    return is_ordered

def draw_list(l):
    plt.plot(l, marker='o', linestyle='-')
    plt.show()
    plt.close()

################################################################################
################################################################################
################################################################################

def random_sort(random_list):
    iteration = 1
    while not check_is_list_ordered(random_list):
        i = randint(0, len(random_list)-1)
        j = randint(0, len(random_list)-1)
        if i != j:
            random_list[i], random_list[j] = random_list[j], random_list[i]
        print(iteration, "|", random_list)
        iteration += 1

################################################################################

def insertion_sort(random_list):
    for i in range(len(random_list)):
        j = i
        insert_value = random_list[i]
        while j > 0 and random_list[j-1] > insert_value:
            random_list[j] = random_list[j-1]
            j -= 1
        random_list[j] = insert_value
        # print(random_list)

################################################################################

def selection_sort(random_list):
    for i in range(len(random_list)-1): # the last element will be the biggest anyway so len - 1
        min_index = i
        for j in range(i+1, len(random_list)): # i+1 because otherwise you compare i with itself
            if random_list[j] < random_list[min_index]:
                min_index = j;
            j += 1;
        random_list[i], random_list[min_index] = random_list[min_index], random_list[i]
        # print(random_list)

################################################################################

def quick_sort_range(random_list, start, end):
    pivot = random_list[int((end-start)/2)]
    print("quicksort from {} to {} and pivot is {}".format(start, end, pivot))
    i = start
    j = end
    while i < j:
        while random_list[i] < pivot:
            print("\t i: {} ({})".format(i, random_list[i]))
            i += 1
        while random_list[j] > pivot:
            print("\t j: {} ({})".format(j, random_list[j]))
            j -= 1
        if i < j:
            print("{} | swap: {} ({}) <-> {}({})".format(pivot, i, random_list[i], j, random_list[j]))
            random_list[i], random_list[j] = random_list[j], random_list[i]
            i += 1
            j -= 1
    if start < j:
        quick_sort_range(random_list, start, j)
    if end > i:
        quick_sort_range(random_list, i, end)

# We do not use <= >= instead of < > because the place where the array will be cut in 2 is not known
# We cannot use pivot (in case of duplicates) or pivot_index, because the split location is not known
# Therefore we opt to swap pivots as a minor tradeof
    
def quick_sort(random_list):
    quick_sort_range(random_list, 0, len(random_list)-1)
    

################################################################################
################################################################################
################################################################################

print("Generating random list...")

# random_list = [3, 7, 4, 0, 9, 5, 2, 6, 8, 1]
random_list = create_random_list(12, 0, 9)
draw_list(random_list)

is_list_ordered = check_is_list_ordered(random_list)
while is_list_ordered:
    print("The list is ordered, generating new random list...")
    random_list = create_random_list(1000000, 0, 9)
    print(random_list)
    is_list_ordered = check_is_list_ordered(random_list)

print("Sorting...")
quick_sort(random_list)

if (not check_is_list_ordered(random_list)):
    print("FAILED")
else:
    print("SUCCES")
draw_list(random_list)
