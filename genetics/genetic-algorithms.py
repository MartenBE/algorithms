
#!/usr/bin/env python3

from random import randint, random
from operator import itemgetter
import matplotlib.pyplot as plt

class Chromosome:

    MUTATE_TRESHOLD = 0.9

    def __init__(self, chromosome):
        self.chromosome = chromosome

    def calculate_cost(self, target):
        cost = 0.0
        for i in range(len(self.chromosome)):
            cost += (ord(target[i]) - ord(self.chromosome[i]))**2
        return cost

    def mutate(self):
        chance = random()
        if chance > Chromosome.MUTATE_TRESHOLD:
            self_new = ""
            pivot = randint(0, len(self.chromosome)-1)
            for i in range(len(self.chromosome)):
                if i != pivot:
                    self_new += self.chromosome[i]
                else:
                    self_new += generate_random_char()
            # print("Mutated (chance={}, treshold={}) from {} to {} at {}".format(chance, Chromosome.MUTATE_TRESHOLD, self.chromosome, self_new, pivot))
            self.chromosome = self_new

    def __str__(self):
        return "Chromosome({})".format(self.chromosome)

class Population:

    DIVIDE_FACTOR = 4

    def __init__(self, size, chromosome_size):
        self.size = size
        self.chromosomes = []
        for i in range(self.size):
            c = generate_random_chromosome(chromosome_size)
            self.chromosomes.append(c)

    def __str__(self):
        output = "Population of size {}\n".format(self.size)
        for c in self.chromosomes:
            output += "\t {}\n".format(c)
        return output

    def calculate_cost(self, target):
        min = (self.chromosomes[0]).calculate_cost(target)
        for c in self.chromosomes:
            if c.calculate_cost(target) < min:
                min =  c.calculate_cost(target)
        return min

    @staticmethod
    def crossover(mom, dad):
        pivot = randint(0, min(len(mom.chromosome)-1, len(dad.chromosome)-1))
        child_chromosome = ""
        for i in range(len(mom.chromosome)):
            if i < pivot:
                child_chromosome += mom.chromosome[i]
            else:
                child_chromosome += dad.chromosome[i]
        child = Chromosome(child_chromosome)
        # print("Crossover between {} and {} with pivot {} gives {}".format(mom.chromosome, dad.chromosome, pivot, child.chromosome))
        return child

    # uniform product distribution
    @staticmethod
    def random_elite_parent(generation):
        return generation[int(random() * random() * (len(generation) - 1))][0]

    def evolve(self, target):
        generation = []
        for c in self.chromosomes:
            generation.append([c, c.calculate_cost(target)])

        generation = sorted(generation, key=itemgetter(1))

        elite_last_index = int(self.size/Population.DIVIDE_FACTOR)
        generation = generation[:elite_last_index]
        children = []
        while len(children) < self.size - elite_last_index:
            child = self.crossover(self.random_elite_parent(generation), self.random_elite_parent(generation))
            child.mutate()
            children.append([child, child.calculate_cost(target)])

        self.chromosomes = []
        for g in generation:
            self.chromosomes.append(g[0])
        for c in children:
            self.chromosomes.append(c[0])

################################################################################

def generate_random_char():
    return chr(randint(32, 126))

def generate_random_chromosome(size):
    chromosome = ""
    for i in range(size):
        chromosome += generate_random_char()
    return Chromosome(chromosome)

################################################################################

# TARGET = "Hello World!"
TARGET = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."

# # Simple algorithm, no use of populations, only mutating chromosome
# # Takes about 400000 iterations with a MUTATION_TRESHOLD of 0.75 for Lorem Ipsum
# i = 0
# c = generate_random_chromosome(len(TARGET))
# while c.calculate_cost(TARGET) > 0:
#   m = Chromosome(c.chromosome)
#   m.mutate()
#   if m.calculate_cost(TARGET) < c.calculate_cost(TARGET):
#       c = m
#   print("{:,}".format(i), c)
#   i += 1

p = Population(20, len(TARGET))
print(p)
print(p.calculate_cost(TARGET))

cost = []
while p.calculate_cost(TARGET) > 0:
    p.evolve(TARGET)
    cost.append(p.calculate_cost(TARGET))
    print(p)
    # wait = input("PRESS ENTER TO CONTINUE.")

plt.plot(cost)
plt.show()
plt.close()
