
#!/usr/bin/env python3

# Neural network class created by MartenBE

import numpy as np
import unittest

class neural_network:

    # Rows are equivalent to layers, and columns to neurons in that layer
    #
    # For example
    #
    # 3 -> 4 -> 2 layer network:
    #
    # a1   b1   c1
    # a2   b2   c2
    # a3   b3
    #      b4
    #
    # self.layers = [3, 4, 2]
    #
    # self.weights =
    #   [
    #       [
    #           [bias->b1,  bias->b2, bias->b3, bias->b4],
    #           [a1->b1,    a1->b2,   a1->b3,   a1->b4],
    #           [a2->b1,    a2->b2,   a2->b3,   a2->b4],
    #           [a3->b1,    a3->b2,   a3->b3,   a3->b4]
    #       ],
    #       [
    #           [bias->c1, bias->c2],
    #           [b1->c1,   b1->c2],
    #           [b2->c1,   b2->c2],
    #           [b3->c1,   b3->c2],
    #           [b4->c1,   b4->c2]
    #       ]
    #   ]

    def __init__(self, layers):
        self.layers = layers
        self.weights = []
        for l in range(1, len(layers)):
            random_values = np.random.rand(
                layers[l-1] + 1, # +1 is for bias, 
                layers[l]
            )
            self.weights.append(2 * random_values - 1) # 2*x-1 to make values in [-1, 1]

    def feed_forward(self, inputs):
        return self.calculate_feed_forward_matrix(inputs)[-1]

    def calculate_feed_forward_matrix(self, inputs):
        #
        # inputs = [a1, a2, a3]
        #
        # results =
        #   [
        #       [result_a1, result_a2],
        #       [result_b1, result_b2, result_b3, result_b4],
        #       [result_c1, result_c2]
        #   ]

        inputs = np.array(inputs)
        outputs = None
        results = [inputs]
        for w in self.weights:

            # bias: [a1, a2, a3] becomes [1, a1, a2, a3]
            biased_inputs = np.insert(results[-1], 0, 1)

            #                                                            [bias->b1, bias->b2, bias->b3, bias->b4],
            #                                                            [a1->b1,   a1->b2,   a1->b3,   a1->b4],
            #                                                            [a2->b1,   a2->b2,   a2->b3,   a2->b4],
            #                                                            [a3->b1,   a3->b2,   a3->b3,   a3->b4]
            # [input_b1, input_b2, input_b3, input_b4] = [1, a1, a2, a3]                dotproduct
            #
            # [output_b1, output_b2, output_b3, output_b4] = sigmoid( [input_b1, input_b2, input_b3, input_b4] )

            outputs = self.__sigmoid(np.dot(biased_inputs, w))
            
            results.append(outputs)
            
        return np.array(results)

    def backpropagate(self, inputs, targets, learning_rate):

        # inputs = [a1, a2, a3]
        # targets = [t1, t2]
        #
        inputs = np.array(inputs)
        targets = np.array(targets)

        # results =
        #   [
        #       [result_a1, result_a2],
        #       [result_b1, result_b2, result_b3, result_b4],
        #       [result_c1, result_c2]
        #   ]
        
        results = self.calculate_feed_forward_matrix(inputs)
        
        # deltas =
        #   [
        #       [delta_a1, delta_a2, delta_a3],
        #       [delta_b1, delta_b2, delta_b3, delta_b4],
        #       [delta_c1, delta_c2]
        #   ]
        
        deltas = [
            (results[-1] - targets) * results[-1] * (1 - results[-1]) # output neurons
        ]

        # hidden layer neurons
        for layer_index in reversed(range(0, (len(self.layers) - 1))):

            #                                                                   weights without bias
            #
            #                                                                [b1->c1, b2->c1, b3->c1, b4->c1],
            #                                                                [b1->c2, b2->c2, b3->c2, b4->c2]
            # [delta_b1, delta_b2, delta_b3, delta_b4] = [delta_c1, delta_c2]            dotproduct           * [result_b1, result_b2, result_b3, result_b4] * ( 1 - [result_b1, result_b2, result_b3, result_b4] )
            #
            
            deltas.append (
                np.dot(deltas[-1], self.weights[layer_index][1:].T) * results[layer_index] * (1 - results[layer_index])
            )
            
        deltas.reverse()
        
        # deltas =
        #   [
        #       [delta_a1, delta_a2],
        #       [delta_b1, delta_b2, delta_b3, delta_b4],
        #       [delta_c1, delta_c2]
        #   ]
        #                                                                           [delta_b1, delta_b2, delta_b3, delta_b4]
        # [bias->b1,  bias->b2, bias->b2, bias->b2],                   [1],        
        # [a1->b1,    a1->b2,   a1->b2,   a1->b2],                     [result_a1],
        # [a2->b1,    a2->b2,   a2->b2,   a2->b2],   - learning_rate * [result_a1],               dotproduct
        # [a3->b1,    a3->b2,   a3->b2,   a3->b2]                      [result_a2]
        
        for w, r, d in zip(self.weights, results, deltas[1:]):
            biased_r = np.insert(r, 0, 1)
            gradient = np.dot(
                np.atleast_2d(biased_r).T, 
                np.atleast_2d(d)
            ) 
            # must use np.atleast_2d to make sure transpose (.T) works
            # "Transposing a 1-D array returns an unchanged view of the original array."
            # http://docs.scipy.org/doc/numpy/reference/generated/numpy.transpose.html
            
            w -= learning_rate * gradient

    def __str__(self):
        s = "Layers: "
        for l in self.layers:
            s += str(l) + " "
        for w in self.weights:
            s += "\n\n" + str(w)
            s += "\n"
        return s

    def __sigmoid(self, x):
        return (1 / (1 + np.exp(-x)))



class test_neural_network(unittest.TestCase):

    def test_init(self):
        nn = neural_network([3, 4, 2])
        self.assertEqual(len(nn.weights), 2)
        self.assertEqual(nn.weights[0].shape, (4, 4))
        self.assertEqual(nn.weights[1].shape, (5, 2))

    def test_feed_forward(self):
        # using data from http://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/
        
        nn = neural_network([2, 2, 2])

        nn.weights[0] = np.array(
            [
                [0.35, 0.35],
                [0.15, 0.25],
                [0.2, 0.3]
            ]
        )

        nn.weights[1] = np.array(
            [
                [0.60, 0.60],
                [0.40, 0.50],
                [0.45, 0.55]
            ]
        )
        
        inputs = [0.05, 0.10]
        
        expected_results = [
            0.75136507, 
            0.772928465
        ]

        results = nn.feed_forward(inputs)

        self.assertTrue(
            np.allclose(
                results,
                expected_results
            ),
            msg = "results are {}, but should be {}".format(
                results, 
                expected_results
            )
        )
        
    def test_backpropagation(self):
        EPOCHS = 10000
        LEARNING_RATE = 0.3
        nn = neural_network([2, 2, 1])

        inputs = [
            [0, 0],
            [1, 0],
            [0, 1],
            [1, 1]
        ]

        targets = [
            [0],
            [1],
            [1],
            [0]
        ]
        
        expected_results = [
            [0.1],
            [0.9],
            [0.9],
            [0.1]
        ]

        for i in range(EPOCHS):
            for input, target in zip(inputs, targets):
                nn.backpropagate(input, target, LEARNING_RATE)
        
        for input, target, expected in zip(inputs, targets, expected_results):
            result = nn.feed_forward(input)
            print("{} -> {} (expected {})".format(
                    input, 
                    result, 
                    expected
                )
            )
            self.assertTrue(
                np.allclose(
                    result,
                    expected,
                    atol = 0.1
                ),
                msg = "results are {} -> {}, but shoud be {}".format(
                    input, 
                    result, 
                    expected
                )
            )

    def test_str(self):
        nn = neural_network([3, 4, 2])
        print(str(nn))

if __name__ == '__main__':
    unittest.main(verbosity=2)