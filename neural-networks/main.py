
#!/usr/bin/env python3

import numpy as np
import nn

TEST_IMAGES_FILENAME = "t10k-images.idx3-ubyte"
TEST_LABELS_FILENAME = "t10k-labels.idx1-ubyte"
TRAIN_IMAGES_FILENAME = "train-images.idx3-ubyte"
TRAIN_LABELS_FILENAME = "train-labels.idx1-ubyte"

print("Reading MNIST files")
train_images = mnist.read_mnist_images(TRAIN_IMAGES_FILENAME) # 60000 x 784
train_labels = mnist.read_mnist_labels(TRAIN_LABELS_FILENAME) # 60000 x 1
test_images = mnist.read_mnist_images(TEST_IMAGES_FILENAME) # 10000 x 784
test_labels = mnist.read_mnist_labels(TEST_LABELS_FILENAME) # 10000 x 1
print("Done...")

AMOUNT_TRAIN_IMAGES = train_images.shape[0]
AMOUNT_TEST_IMAGES = test_images.shape[0]

# print(train_images.shape)

### 784 > 1000 > 10



