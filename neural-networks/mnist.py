
#!/usr/bin/env python3

import struct
import numpy as np
import matplotlib.pyplot as plt

def read_mnist_images(images_filename):
	images = np.zeros([])
	with open(images_filename, "rb") as f:
		MAGIC = struct.unpack('>I', f.read(4))[0]
		SIZE = struct.unpack('>I', f.read(4))[0]
		ROWS = struct.unpack('>I', f.read(4))[0]
		COLUMNS = struct.unpack('>I', f.read(4))[0]
		AMOUNT_PIXELS = ROWS * COLUMNS

		images = np.zeros([SIZE, AMOUNT_PIXELS])

		byte = f.read(1)
		for i in range(0, SIZE):
			for j in range(0, AMOUNT_PIXELS):
				images[i, j] = ord(byte)
				byte = f.read(1)
	return images

def read_mnist_labels(labels_filename):
	labels = np.zeros([])
	with open(labels_filename, "rb") as f:
		MAGIC = struct.unpack('>I', f.read(4))[0]
		SIZE = struct.unpack('>I', f.read(4))[0]

		labels = np.zeros([SIZE])

		byte = f.read(1)
		for i in range(0, SIZE):
			labels[i] = ord(byte)
			byte = f.read(1)
	return labels

def display_mnist_image(pixel_array, rows, columns):
	mat = np.zeros([rows, columns])
	i = 0;
	for row in range(0, rows):
		for col in range(0, columns):
			mat[row, col] = pixel_array[i]
			i += 1
	plt.matshow(mat)
	plt.show()
