
#!/usr/bin/env python3

#

text_file = open("bible.txt", 'r')
text = text_file.read()
text_file.close()

search = "General"

partial_table = []

for i in range(len(search)):
	partial_table.append(0)

for i in range(1, len(search)):
	j = partial_table[i-1] # get the longest prefix of the previous character

	# Does the previous char has a prefix? (j > 0)
	# Now does the next char after this prefix match the current char? (search[i] != search[j])
	# The length of the prefix is the index of the next char after that prefix (j)
	# keep hopping to smaller prefixes until it fits
	while j > 0 and search[i] != search[j]:
		j = partial_table[j-1]

	# does it fit now? Add the current char to the prefix
	if search[i] == search[j]:
		j += 1
	partial_table[i] = j

for i in range(len(search)):
	print(search[i], end=' ')

print()

for i in range(len(partial_table)):
	print(partial_table[i], end=' ')

print()

matched = 0
found = False
i = 0

while i < len(text) and not found:
	print(text[i], end='')
	# try other prefixes
	while matched > 0 and text[i] != search[matched]:
		matched = partial_table[matched-1]
	# allright it matches, on to the next char!
	if text[i] == search[matched]:
		matched += 1
		if matched == len(search):
			found = True
	i += 1

if i < len(text):
	print(" <<< FOUND")
else:
	print()
	print(">>> NOT FOUND <<<")
