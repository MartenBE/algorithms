
import matplotlib.pyplot as plt
import numpy as np

x = np.arange(-3, 3, 0.1);

y = -np.log(1 / (1 + np.exp(-x)))
plt.plot(x, y)
plt.show()

y = -np.log(1 - (1 / (1 + np.exp(-x))))
plt.plot(x, y)
plt.show()