
# http://www.algolist.net/Algorithms/Graph/Undirected/Depth-first_search

from matplotlib import pyplot as plt
import networkx as nx
from collections import deque
import graph

g = graph.Graph()

n1 = graph.Node(1)
n2 = graph.Node(2)
n3 = graph.Node(3)
n4 = graph.Node(4)
n5 = graph.Node(5)
n6 = graph.Node(6)
n7 = graph.Node(7)

g.add_node(n1)
g.add_node(n2)
g.add_node(n3)
g.add_node(n4)
g.add_node(n5)
g.add_node(n6)
g.add_node(n7)

g.add_undirected_connection(n1, n2)
g.add_undirected_connection(n1, n3)
g.add_undirected_connection(n1, n5)
g.add_undirected_connection(n2, n4)
g.add_undirected_connection(n2, n6)
g.add_undirected_connection(n3, n7)
g.add_undirected_connection(n5, n6)

print(g)
g.show_plot()

### DFS

print("DFS")
print("---")

nodes_to_process = [n1] # stack
processed_nodes = []
while len(nodes_to_process) > 0:
    current_node = nodes_to_process.pop()
    if (current_node not in processed_nodes):
        for n in g.get_neighbours(current_node):
            nodes_to_process.append(n)
        processed_nodes.append(current_node)
        print(current_node)

print()

### BFS

print("BFS")
print("---")

nodes_to_process = deque([n1]) # queue
processed_nodes = []
while len(nodes_to_process) > 0:
    current_node = nodes_to_process.popleft()
    if (current_node not in processed_nodes):
        for n in g.get_neighbours(current_node):
            nodes_to_process.append(n)
        processed_nodes.append(current_node)
        print(current_node)

print()
