from matplotlib import pyplot as plt
import networkx as nx
from collections import deque

class Node:
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "Node(" + str(self.value) + ")"

class Graph:
    def __init__(self):
        self.nodes = []
        self.adj_list = {}

    def add_node(self, Node):
        self.nodes.append(Node)
        self.adj_list[Node] = []

    def add_directed_connection(self, Node1, Node2):
        self.adj_list[Node1].append(Node2)

    def add_undirected_connection(self, Node1, Node2):
        self.adj_list[Node1].append(Node2)
        self.adj_list[Node2].append(Node1)

    def get_neighbours(self, Node):
        return self.adj_list[Node]

    def show_plot(self):
        g = nx.Graph()
        labels = {}
        for n in self.nodes:
            g.add_node(n)
            labels[n] = n.value
            for a in self.adj_list[n]:
                g.add_edge(n, a)
        pos = nx.spring_layout(g)
        nx.draw_networkx_labels(g, pos, labels=labels)
        nx.draw_networkx_nodes(g,pos)
        nx.draw_networkx_edges(g,pos)
        plt.show()


    def __str__(self):
        output = ""
        for n in self.nodes:
            output += str(n)
            output += " connected with:"
            for a in self.adj_list[n]:
                output += " "
                output += str(a)
            output += "\n"
        return output
