
#!/usr/bin/env python3

# http://www.theprojectspot.com/tutorial-post/applying-a-genetic-algorithm-to-the-travelling-salesman-problem/5
# http://katrinaeg.com/simulated-annealing.html

import csv
import matplotlib.pyplot as plt
import math
import random
import copy
import time

################################################################################

class City:

	def __init__(self, x, y):
		self.x = x
		self.y = y

	def distance(self, city):
		dx = abs(self.x - city.x)
		dy = abs(self.y - city.y)
		return math.sqrt(dx**2 + dy**2)

	def __str__(self):
		return "City(x={}, y={})".format(self.x, self.y)

class Route:

	def __init__(self, cities):
		self.cities = cities

	def add_city(self, city):
		self.cities.append(city)

	def print_cities(self):
		for c in self.cities:
			print(c)

	def draw_cities(self):
		plt.scatter([c.x for c in self.cities], [c.y for c in self.cities])
		plt.show()
		plt.close()

	def draw_route(self):
		plt.scatter([c.x for c in self.cities], [c.y for c in self.cities])
		last_city_index = len(self.cities) - 1
		for i in range(0, last_city_index):
			from_x = self.cities[i].x
			from_y = self.cities[i].y
			to_x = self.cities[i+1].x
			to_y = self.cities[i+1].y
			plt.plot((from_x, to_x), (from_y, to_y))
		from_x = self.cities[last_city_index].x
		from_y = self.cities[last_city_index].y
		to_x = self.cities[0].x
		to_y = self.cities[0].y
		plt.plot((from_x, to_x), (from_y, to_y))
		plt.show()
		plt.close()

	def distance_cities_by_index(self, i, j):
		return self.cities[i].distance(self.cities[j])

	def distance(self):
		distance = 0
		last_city_index = len(self.cities) - 1
		for i in range(0, last_city_index):
			distance += self.distance_cities_by_index(i, i+1)
		distance += self.distance_cities_by_index(last_city_index, 0)
		return distance

	def shuffle(self):
		random.shuffle(self.cities)

	def random_single_swap(self):
		i = random.randint(0, len(self.cities)-1)
		j = random.randint(0, len(self.cities)-1)
		while i == j:
			j = random.randint(0, len(self.cities)-1)
			self.cities[i], self.cities[j] = self.cities[j], self.cities[i]

################################################################################
### I/O


def read_cities(filename):
	f = open(filename, 'r')
	csv_f = csv.reader(f)
	cities = []
	for row in csv_f:
		cities.append(City(int(row[0].strip()), int(row[1].strip())))
	return cities

################################################################################
### Simulated annealing

def route_cost(route):
	return route.distance()

def acceptance_probability(neighbor_cost, cost, temp, verbose=False):
	if neighbor_cost < cost:
		if verbose:
			print("\tBetter result")
		return 1;
	else:
		prob = math.exp((cost-neighbor_cost)/temp)
		if verbose:
			print("\tWorse result", prob)
	return prob

def simulated_annaeling(route, verbose=False):
	print("Running simulated annealing, this could take a while...")

	start_time = time.time()
	temp = 100
	ALPHA = 0.9999
	MIN_TEMP = 1

	distances = []
	while temp > MIN_TEMP:
		for i in range(10):
			neighbor = copy.deepcopy(route)
			neighbor.random_single_swap()
			neighbor_cost = route_cost(neighbor)
			cost = route_cost(route)
			if acceptance_probability(neighbor_cost, cost, temp, verbose) > random.random():
				if verbose:
					print("\tjump")
				route = neighbor
			if verbose:
				print(temp, "| Distance is", route.distance())
			distances.append(route.distance())
		temp *= ALPHA
	end_time = time.time()

	plt.plot(distances)
	plt.show()
	plt.close()

	route.print_cities()
	route.draw_route()
	print("Done (elapsed time:", end_time - start_time, ")", "Optimal distance is", route.distance())

################################################################################

r = Route(read_cities("cities.txt"))

# random init
# r.shuffle()
# r.draw_route()
print("Initial distance is", r.distance())

simulated_annaeling(r, True)
